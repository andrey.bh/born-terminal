<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);
require 'vendor/autoload.php';

use Rapa\Born\Terminal;

$pricingData = [
    [
        'code'              => 'A',
        'price'             => 2,
        'discount_count'    => 4,
        'discount_cost'     => 7,
    ],
    [
        'code'              => 'B',
        'price'             => 12,
    ],
    [
        'code'              => 'C',
        'price'             => 1.25,
        'discount_count'    => 6,
        'discount_cost'     => 6,
    ],
    [
        'code'              => 'D',
        'price'             => 0.15,
    ],
];

$list = [
    'ABCDABAA',
    'CCCCCCC',
    'ABCD'
];

/**
 * Run test
 *
 * @param string $title
 * @param string $cart
 * @param Terminal $terminal
 */
function run(
    string $title,
    string $cart,
    Terminal $terminal
) {
    printf("*** %s ***\n\n", $title);
    $terminal->reset();
    for ($i = 0; $i < strlen($cart); $i++) {
        printf("Scanning \"%s\": ", $cart[$i]);
        try {
            $terminal->scan($cart[$i]);
            print "OK\n";
        } catch (\Exception $e) {
            print $e->getMessage() ."\n";
        }
    }

    printf(
        "\nTotal cost: $%s\n\n",
        number_format($terminal->total, 2)
    );
}

try {
    $terminal= new Terminal();
    $terminal->setPricing($pricingData);
    foreach ($list as $n => $cart) {
        run(
            sprintf('Test %d', $n+1),
            $cart,
            $terminal
        );
    }
} catch (\Exception $e) {
    printf(
        "ERROR: %s\n\n",
        $e->getMessage()
    );
}

