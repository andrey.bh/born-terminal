<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
namespace Rapa\Born;

interface ItemInterface
{
    /**
     * Increment item count
     *
     * @param int $value
     * @return ItemInterface
     */
    public function add(int $value = 1): ItemInterface;

    /**
     * Returns count
     *
     * @return int
     */
    public function getCount(): int;

    /**
     * Returns discount cost
     *
     * @return float
     */
    public function getDiscountCost(): float;

    /**
     * Returns discount count
     *
     * @return int
     */
    public function getDiscountCount(): int;

    /**
     * Returns price
     *
     * @return float
     */
    public function getPrice(): float;

    /**
     * Returns total cost
     *
     * @param bool $forced
     * @return float
     */
    public function getTotal(bool $forced = false): float;

    /**
     * Reset data
     *
     * @return ItemInterface
     */
    public function reset(): ItemInterface;

    /**
     * Sets count value
     *
     * @param int $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setCount(int $value): ItemInterface;

    /**
     * Sets discount cost value
     *
     * @param float $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setDiscountCost(float $value): ItemInterface;

    /**
     * Sets discount count value
     *
     * @param int $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setDiscountCount(int $value): ItemInterface;

    /**
     * Sets price value
     *
     * @param float $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setPrice(float $value): ItemInterface;
}