<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
namespace Rapa\Born;

class Terminal implements TerminalInterface
{
    const ERROR_CODE            = 'Incorrect item code value "%s". The value should be a char.';
    const ERROR_ITEM_NOT_FOUNT  = 'The item "%s" not found.';

    /**
     * @var bool
     */
    protected $_changed         = false;

    /**
     * @var array
     */
    protected $_items           = [];

    /**
     * @var float
     */
    protected $_total           = 0;

    /**
     * Returns item by code
     *
     * @param string $code
     * @return false|Item
     */
    protected function _getItem(string $code)
    {
        return $this->_items[$code] ?? false;
    }

    /**
     * Validate item code value
     *
     * @param string $code
     * @return string
     * @throws \Exception
     */
    protected function _validateCode(string $code): string
    {
        if (strlen($code) !== 1) {
            throw new \Exception(sprintf(
                self::ERROR_CODE,
                $code
            ));
        }

        return strtoupper($code);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $name == 'total' ? $this->getTotal() : null;
    }

    /**
     * Returns total cost
     *
     * @param bool $forced
     * @return float
     */
    public function getTotal(bool $forced = false): float
    {
        if ($this->_changed || $forced) {
            $this->_total = 0;
            foreach ($this->_items as $item) {
                $this->_total += $item->getTotal();
            }
        }

        return (float) $this->_total;
    }

    /**
     * Reset data
     *
     * @return TerminalInterface
     */
    public function reset(): TerminalInterface
    {
        $this->_total = 0;
        $this->_changed = false;

        foreach ($this->_items as $item) {
            $item->reset();
        }

        return $this;
    }

    /**
     * Scanning product
     *
     * @param string $value
     * @return TerminalInterface
     * @throws \Exception
     */
    public function scan(string $value): TerminalInterface
    {
        if (!$item = $this->_getItem($value)) {
            throw new \Exception(sprintf(
                static::ERROR_ITEM_NOT_FOUNT,
                $value
            ));
        }

        $item->add();
        $this->_changed = true;

        return $this;
    }

    /**
     * Sets pricing data
     *
     * @param array $value
     * @return TerminalInterface
     * @throws \Exception
     */
    public function setPricing(array $value): TerminalInterface
    {
        $this->_changed = true;
        foreach ($value as $item) {
            $code = $this->_validateCode($item['code'] ?? '');
            $this->_items[$code] = new Item($item);
        }

        return $this;
    }
}