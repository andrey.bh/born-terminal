<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
namespace Rapa\Born;

interface TerminalInterface
{
    /**
     * Returns total cost
     *
     * @param bool $forced
     * @return float
     */
    public function getTotal(bool $forced = false): float;

    /**
     * Reset data
     *
     * @return TerminalInterface
     */
    public function reset(): TerminalInterface;

    /**
     * Scanning product
     *
     * @param string $value
     * @return TerminalInterface
     * @throws \Exception
     */
    public function scan(string $value): TerminalInterface;

    /**
     * Sets pricing data
     *
     * @param array $value
     * @return TerminalInterface
     * @throws \Exception
     */
    public function setPricing(array $value): TerminalInterface;
}