<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
namespace Rapa\Born;

class Item implements ItemInterface
{
    const ERROR_NEGATIVE_VALUE  = 'The "%s" value can\'t be negative.';

    /**
     * @var int
     */
    protected $_count           = 0;

    /**
     * @var bool
     */
    protected $_changed         = false;

    /**
     * @var float
     */
    protected $_discountCost    = 0;

    /**
     * @var int
     */
    protected $_discountCount   = 0;

    /**
     * @var float
     */
    protected $_price           = 0;

    /**
     * @var float
     */
    protected $_total           = 0;

    /**
     * Calculate total price
     *
     * @return ItemInterface
     */
    public function _calculate(): ItemInterface
    {
        if ($this->getDiscountCount()) {
            $this->_total = intdiv($this->getCount(), $this->getDiscountCount()) * $this->getDiscountCost() +
                ($this->getCount() % $this->getDiscountCount()) * $this->getPrice();
        } else {
            $this->_total = $this->getCount() * $this->getPrice();
        }

        return $this;
    }

    /**
     * Returns setting method name by code
     *
     * @param string $value
     * @return string
     */
    public function _getMethodName(string $value): string
    {
        return 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($value))));
    }

    /**
     * Checks if value is valid
     *
     * @param int|float $value
     * @return int|float
     * @throws \Exception
     */
    protected function _validateValue(string $name, $value)
    {
        if ($value < 0) {
            throw new \Exception(sprintf(
                static::ERROR_NEGATIVE_VALUE,
                $name
            ));
        }

        return $value;
    }

    /**
     * Item constructor.
     *
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $name => $value) {
            $method = $this->_getMethodName($name);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * Increment item count
     *
     * @param int $value
     * @return ItemInterface
     */
    public function add(int $value = 1): ItemInterface
    {
        $this->_count += $value;
        $this->_changed = true;

        return $this;
    }

    /**
     * Returns count
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->_count;
    }

    /**
     * Returns discount cost
     *
     * @return float
     */
    public function getDiscountCost(): float
    {
        return (float) $this->_discountCost;
    }

    /**
     * Returns discount count
     *
     * @return int
     */
    public function getDiscountCount(): int
    {
        return $this->_discountCount;
    }

    /**
     * Returns price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return (float) $this->_price;
    }

    /**
     * Returns total cost
     *
     * @param bool $forced
     * @return float
     */
    public function getTotal(bool $forced = false): float
    {
        if ($this->_changed || $forced) {
            $this->_calculate();
        }

        return $this->_total;
    }

    /**
     * Reset data
     *
     * @return ItemInterface
     */
    public function reset(): ItemInterface
    {
        $this->_total = 0;
        $this->_count = 0;
        $this->_changed = false;

        return $this;
    }

    /**
     * Sets count value
     *
     * @param int $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setCount(int $value): ItemInterface
    {
        $this->_count = $this->_validateValue('count', $value);
        $this->_changed = true;

        return $this;
    }

    /**
     * Sets discount cost value
     *
     * @param float $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setDiscountCost(float $value): ItemInterface
    {
        $this->_discountCost = $this->_validateValue('discount cost', $value);
        $this->_changed = true;

        return $this;
    }

    /**
     * Sets discount count value
     *
     * @param int $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setDiscountCount(int $value): ItemInterface
    {
        $this->_discountCount = $this->_validateValue('discount count', $value);
        $this->_changed = true;

        return $this;
    }

    /**
     * Sets price value
     *
     * @param float $value
     * @return ItemInterface
     * @throws \Exception
     */
    public function setPrice(float $value): ItemInterface
    {
        $this->_price = $this->_validateValue('price', $value);
        $this->_changed = true;

        return $this;
    }
}